## Insert your Dockerfile directives here to build the proper Docker image
FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
ONBUILD . /app